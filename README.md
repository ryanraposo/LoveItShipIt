<p align="center">
  <a href="">
    <img src="https://gist.githubusercontent.com/ryanraposo/594168edd80d76499d4fcb0693ccd2fc/raw/dd37acb08728631f816c4ef61cc39d37986c32a6/sticker02.svg" width="300">
  </a>
</p>

```markdown
[![LoveIt;ShipIt](https://gist.githubusercontent.com/ryanraposo/594168edd80d76499d4fcb0693ccd2fc/raw/dd37acb08728631f816c4ef61cc39d37986c32a6/sticker02.svg)]()
```

# Example

[LoveIt;ShipIt](https://gitlab.com/ryanraposo/LoveItShipIt) is a reaffirmation from maintainers who want their contributor counterparts to know that it is...

    1. The order of operations in the project, and so better stands to be.
    2. The standard for all changes, and so better stands to be.
    3. The ideal communication between members of the project, above all else.

The maintainer(s) of [project] have reaffirmed with honest belief...

    1. That these reaffirmations preserve the true value of the project.
    2. That contributors can expect the highest possible degree of respect as a result.
    3. That doing so is the surest way to invoke the sentiment from users.

# Raw

```markdown
[LoveIt;ShipIt](https://gitlab.com/ryanraposo/LoveItShipIt) is a reaffirmation from maintainers who want their contributor counterparts to know that it is...

    1. The order of operations in the project, and so better stands to be.
    2. The standard for all changes, and so better stands to be.
    3. The ideal communication between members of the project, above all else.

The maintainer(s) of [project] have reaffirmed with honest belief...

    1. That these reaffirmations preserve the true value of the project.
    2. That contributors can expect the highest possible degree of respect as a result.
    3. That doing so is the surest way to invoke the sentiment from users.
```

# Notice

Use where/when needed.
